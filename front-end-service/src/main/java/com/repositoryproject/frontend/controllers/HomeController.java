package com.repositoryproject.frontend.controllers;

import com.repositoryproject.frontend.clients.myrepos.MyRepoClient;
import com.repositoryproject.frontend.clients.myrepos.MyRepoCreateDTO;
import com.repositoryproject.frontend.clients.reviews.ReviewDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
@RequestMapping("/")
public record HomeController(MyRepoClient myRepoClient) {

    @GetMapping
    public String getHomePage(Model model) {

        model.addAllAttributes(Map.of(
                "allRepos", myRepoClient.getAll(),
                "createRepoDto", new MyRepoCreateDTO(),
                "createReviewDto", new ReviewDto()));
        return "index";
    }
}
