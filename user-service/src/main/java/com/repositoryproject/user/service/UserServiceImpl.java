package com.repositoryproject.user.service;

import com.repositoryproject.user.model.MyUser;
import com.repositoryproject.user.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public record UserServiceImpl(UserRepository userRepository) implements UserService {

    @Override
    public List<MyUser> getAll() {
        return userRepository.findAll();
    }
}
