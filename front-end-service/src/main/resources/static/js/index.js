const repositoryReviewsModal = $('#repo-review-modal');
$("#create-repo").click(createRepo);
$("#add-review-button").click(addReview);
$("#delete-review-button").click(deleteReview);

function openRepositoryReviewsModal(value) {
    $('#selected-review-id').val(value);
    $('.review-results').load(`/reviews/${value}`, checkIfSuccessModal(repositoryReviewsModal));
}

function createRepo(event) {
    event.preventDefault();
    let form = $("#create-repo-form");
    let formData = extractFormData(form);
    $(".repo-results").load(`/repositories`, formData, checkIfSuccess(`Successfully added repo ${formData.name}!`));
    $("#create-repo-modal").modal('hide');
}

function addReview(event) {
    event.preventDefault();
    let form = $("#add-review-form");
    let formData = extractFormData(form);
    $(".review-results").load("/reviews", formData, checkIfSuccess("Successfully added review!"));
    $("#rate-repo-modal").modal('hide');
}

function closeDeleteConfirm(reviewId) {
    $("#modal-confirm-delete" + reviewId).modal('hide');
    repositoryReviewsModal.modal('show')
}

function openDeleteConfirm(reviewId) {
    let modalConfirmDelete = $("#modal-confirm-delete" + reviewId);
    modalConfirmDelete.prependTo("#repo-review-modal");
    modalConfirmDelete.modal("show");
}

function deleteReview(reviewId) {
    let modalConfirmDelete = $("#modal-confirm-delete" + reviewId);
    let deleteReviewForm = $("#delete-review-form" + reviewId);
    modalConfirmDelete.prependTo("body");
    modalConfirmDelete.modal('hide');
    $(".review-results").load(deleteReviewForm.attr("action"), {}, checkIfSuccess("Successfully deleted review!"));
    repositoryReviewsModal.modal("show");
}

// HELPER METHODS
function extractFormData(form) {
    let output = {};
    $.each(form.serializeArray(), (_, kv) => output[kv.name] = kv.value);
    return output;
}

function checkIfSuccessModal(modalToLoad) {
    return (responseTxt, statusTxt, xhr) =>
        statusTxt === "success" ? modalToLoad.modal('show') : vt.error(xhr.responseText, {});
}

function checkIfSuccess(text) {
    return (responseTxt, statusTxt, xhr) =>
        statusTxt === "success" ? vt.success(text, {}) : vt.error(xhr.responseText, {});
}

