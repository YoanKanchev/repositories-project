package com.repositoryproject.frontend.clients.users;

import com.repositoryproject.frontend.config.OAuthFeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "user-service", configuration = OAuthFeignConfig.class)
public interface UserClient {

    @GetMapping("/users")
    List<MyUser> getUsers();
}
