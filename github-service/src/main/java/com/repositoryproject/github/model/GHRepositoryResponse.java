package com.repositoryproject.github.model;

import lombok.Builder;

@Builder
public record GHRepositoryResponse(String URL, Boolean isOperationSuccessful, String responseMessage) {
}
