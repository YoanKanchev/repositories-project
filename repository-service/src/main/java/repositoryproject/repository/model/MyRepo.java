package repositoryproject.repository.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MyRepo {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String description;
    private String ownerName;
    private String linkToGitHub;
}
