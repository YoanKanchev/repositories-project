package com.repositoryproject.github.controller;

import com.repositoryproject.github.exceptions.DuplicateEntityException;
import com.repositoryproject.github.model.GHCreateDto;
import com.repositoryproject.github.model.GHRepositoryResponse;
import com.repositoryproject.github.service.GithubService;
import org.kohsuke.github.GHRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/github-repositories")
public record GithubController(GithubService githubService) {

    @PostMapping
    public GHRepositoryResponse create(@RequestBody GHCreateDto ghCreateDto) {
        try {
            GHRepository ghRepository = githubService.create(ghCreateDto);
            return new GHRepositoryResponse(ghRepository.getHtmlUrl().toString(), true, null);
        } catch (IOException | DuplicateEntityException e) {
            return new GHRepositoryResponse(null, false, e.getMessage());
        }
    }
}
