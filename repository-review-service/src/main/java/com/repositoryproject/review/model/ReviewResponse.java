package com.repositoryproject.review.model;

public record ReviewResponse(Boolean isOperationSuccessful, String responseMessage) {
}
