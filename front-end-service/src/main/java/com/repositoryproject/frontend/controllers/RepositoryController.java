package com.repositoryproject.frontend.controllers;

import com.repositoryproject.frontend.clients.myrepos.MyRepoClient;
import com.repositoryproject.frontend.clients.myrepos.MyRepoCreateDTO;
import com.repositoryproject.frontend.services.RepositoryService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/repositories")
public record RepositoryController(RepositoryService repositoryService, MyRepoClient myRepoClient) {

    @PostMapping
    public String createRepo(@ModelAttribute MyRepoCreateDTO createDto, Model model, @AuthenticationPrincipal Jwt principal) {
        createDto.setOwnerName(principal.getClaimAsString("preferred_username"));
        repositoryService.createRepo(createDto);

        model.addAttribute("allRepos", myRepoClient.getAll());
        return "index::repo-results";
    }
}
