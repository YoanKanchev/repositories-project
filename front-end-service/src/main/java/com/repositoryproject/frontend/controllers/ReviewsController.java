package com.repositoryproject.frontend.controllers;

import com.repositoryproject.frontend.clients.reviews.ReviewClient;
import com.repositoryproject.frontend.clients.reviews.ReviewDto;
import com.repositoryproject.frontend.clients.reviews.ReviewResponse;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@Controller
@RequestMapping("/reviews")
public record ReviewsController(ReviewClient reviewClient) {

    @GetMapping("/{repositoryId}")
    public String getAllForRepository(@PathVariable int repositoryId, Model model) {
        model.addAttribute( "allReviewsForRepo", reviewClient.getAllForRepository(repositoryId));
        return "index::review-results";
    }

    @PostMapping
    public String reviewRepository(@Valid @ModelAttribute ReviewDto reviewDto, BindingResult bindingResult, Model model, @AuthenticationPrincipal Jwt principal) {
        assert !bindingResult.hasErrors() : "Please enter all mandatory fields!";
        reviewDto.setReviewer(principal.getClaimAsString("preferred_username"));
        ReviewResponse reviewResponse = reviewClient.reviewRepository(reviewDto);
        assert reviewResponse.isOperationSuccessful() : reviewResponse.responseMessage();

        model.addAttribute(
                "allReviewsForRepo", reviewClient.getAllForRepository(reviewDto.getRepositoryId()));
        return "index::review-results";
    }

    @PostMapping("/{repositoryId}/{id}/delete")
    public String delete(@PathVariable int repositoryId, @PathVariable int id, Model model) {
        ReviewResponse reviewResponse = reviewClient.delete(id);
        assert reviewResponse.isOperationSuccessful() : reviewResponse.responseMessage();

        model.addAttribute("allReviewsForRepo", reviewClient.getAllForRepository(repositoryId));
        return "index::review-results";
    }

}
