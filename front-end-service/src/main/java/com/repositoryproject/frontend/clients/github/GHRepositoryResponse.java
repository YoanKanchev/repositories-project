package com.repositoryproject.frontend.clients.github;

public record GHRepositoryResponse(String URL, Boolean isOperationSuccessful, String responseMessage) {
}
