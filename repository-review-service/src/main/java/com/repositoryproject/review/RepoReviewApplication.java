package com.repositoryproject.review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class RepoReviewApplication {
    public static void main(String[] args) {
        SpringApplication.run(RepoReviewApplication.class, args);
    }
}
