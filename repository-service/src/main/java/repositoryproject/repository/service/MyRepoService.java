package repositoryproject.repository.service;

import repositoryproject.repository.model.MyRepo;
import repositoryproject.repository.model.MyRepoCreateDTO;

import java.util.List;

public interface MyRepoService {
    List<MyRepo> getAll();

    MyRepo create(MyRepoCreateDTO createDTO);
}
