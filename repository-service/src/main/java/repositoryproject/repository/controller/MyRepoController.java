package repositoryproject.repository.controller;

import org.springframework.web.bind.annotation.*;
import repositoryproject.repository.model.MyRepo;
import repositoryproject.repository.model.MyRepoCreateDTO;
import repositoryproject.repository.service.MyRepoService;

import java.util.List;

@RestController
@RequestMapping("/my-repositories")
public record MyRepoController(MyRepoService service) {

    @GetMapping
    public List<MyRepo> getAll() {
        return service.getAll();
    }

    @PostMapping
    public MyRepo create(@RequestBody MyRepoCreateDTO createDto) {
        return service.create(createDto);
    }
}
