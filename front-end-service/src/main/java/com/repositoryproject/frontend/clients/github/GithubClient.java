package com.repositoryproject.frontend.clients.github;

import com.repositoryproject.frontend.config.OAuthFeignConfig;
import org.kohsuke.github.GHRepository;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "github-service", configuration = OAuthFeignConfig.class)
public interface GithubClient {

    @PostMapping("/github-repositories")
    GHRepositoryResponse create(@RequestBody GHCreateDto ghCreateDto);
}
