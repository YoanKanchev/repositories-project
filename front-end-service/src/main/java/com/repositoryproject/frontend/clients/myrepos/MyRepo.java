package com.repositoryproject.frontend.clients.myrepos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyRepo {
    private Integer id;
    private String name;
    private String description;
    private String ownerName;
    private String linkToGitHub;
}
