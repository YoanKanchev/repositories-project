package com.repositoryproject.frontend.clients.reviews;

import com.repositoryproject.frontend.config.OAuthFeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@FeignClient(name = "repository-review-service", configuration = OAuthFeignConfig.class)
public interface ReviewClient {

    @GetMapping("/reviews/{repositoryId}")
    List<Review> getAllForRepository(@PathVariable("repositoryId") int repositoryId);

    @PostMapping("/reviews")
    ReviewResponse reviewRepository(@Valid @RequestBody ReviewDto reviewDto);

    @DeleteMapping("/reviews/{id}/delete")
    ReviewResponse delete(@PathVariable("id") int id);
}
