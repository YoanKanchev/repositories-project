package com.repositoryproject.frontend.clients.github;


public record GHCreateDto(String name, String description, String owner, Boolean isPrivate) {
}
