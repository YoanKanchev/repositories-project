package com.repositoryproject.review.model;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class ReviewDto {
    @Max(value = 5)
    @Positive
    @NotNull
    private Integer repositoryId;
    private String reviewer;
    private Double rating;
    private String description;
}
