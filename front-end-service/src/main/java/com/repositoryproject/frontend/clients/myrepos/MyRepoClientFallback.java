package com.repositoryproject.frontend.clients.myrepos;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MyRepoClientFallback implements MyRepoClient {

    @Override
    public List<MyRepo> getAll() {
        return new ArrayList<>();
    }

    @Override
    public MyRepo create(MyRepoCreateDTO createDto) {
        return null;
    }
}
