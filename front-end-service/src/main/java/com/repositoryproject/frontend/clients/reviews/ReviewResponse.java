package com.repositoryproject.frontend.clients.reviews;

public record ReviewResponse(Boolean isOperationSuccessful, String responseMessage) {
}
