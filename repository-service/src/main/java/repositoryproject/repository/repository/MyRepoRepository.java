package repositoryproject.repository.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import repositoryproject.repository.model.MyRepo;

@Repository
public interface MyRepoRepository extends JpaRepository<MyRepo, Integer> {
}
