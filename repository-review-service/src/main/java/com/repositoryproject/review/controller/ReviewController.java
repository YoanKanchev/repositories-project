package com.repositoryproject.review.controller;

import com.repositoryproject.review.exceptions.EntityNotFoundException;
import com.repositoryproject.review.model.Review;
import com.repositoryproject.review.model.ReviewDto;
import com.repositoryproject.review.model.ReviewResponse;
import com.repositoryproject.review.service.ReviewService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/reviews")
public record ReviewController(ReviewService reviewService) {

    @GetMapping("/{repositoryId}")
    public List<Review> getAllForRepository(@PathVariable int repositoryId) {
        return reviewService.getAllForRepository(repositoryId);
    }

    @PostMapping
    public ReviewResponse reviewRepository(@Valid @RequestBody ReviewDto reviewDto) {
        reviewService.reviewRepository(reviewDto);
        return new ReviewResponse(true, "Success");
    }

    @DeleteMapping("/{id}/delete")
    public ReviewResponse delete(@PathVariable int id) {
        try {
            reviewService.delete(id);
        } catch (EntityNotFoundException e) {
            return new ReviewResponse(false, e.getMessage());
        }
        return new ReviewResponse(true, "Success");
    }
}
