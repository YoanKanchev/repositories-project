package repositoryproject.repository.model;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class MyRepoCreateDTO {

    @Size(min = 2, max = 20, message = "Name should be between 2 & 20 symbols")
    private String name;
    private String description;
    private String ownerName;
    private String linkToGitHub;

}
