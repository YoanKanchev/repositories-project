package com.repositoryproject.github.service;

import com.repositoryproject.github.exceptions.DuplicateEntityException;
import com.repositoryproject.github.model.GHCreateDto;
import com.repositoryproject.github.model.GHRepositoryResponse;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.PagedSearchIterable;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public record GithubService(GitHub github) {

    public GHRepository create(GHCreateDto ghCreateDto) throws IOException {
        checkIfRepoExists(ghCreateDto);
        return github
                .createRepository(ghCreateDto.name())
                .description(ghCreateDto.description())
                .owner(ghCreateDto.owner())
                .private_(ghCreateDto.isPrivate())
                .create();
    }

    private void checkIfRepoExists(GHCreateDto ghCreateDto)  {
        try {
//            PagedSearchIterable<GHRepository> gh = searchRepositories().user(ghCreateDto.owner()).list();
            GHRepository ghRepository =  github.getRepository(github.getMyself().getLogin() + "/" + ghCreateDto.name());
            if (ghRepository != null){
                throw new DuplicateEntityException("Github repo with this name already exists");
            }
        } catch (IOException ignored) {
            String test = "";
        }
    }
}
