package com.repositoryproject.frontend.services;

import com.repositoryproject.frontend.clients.github.GHCreateDto;
import com.repositoryproject.frontend.clients.github.GHRepositoryResponse;
import com.repositoryproject.frontend.clients.github.GithubClient;
import com.repositoryproject.frontend.clients.myrepos.MyRepoClient;
import com.repositoryproject.frontend.clients.myrepos.MyRepoCreateDTO;
import com.repositoryproject.frontend.exceptions.BadRequestException;
import org.springframework.stereotype.Service;

@Service
public record RepositoryService(GithubClient githubClient, MyRepoClient myRepoClient) {

    public void createRepo(MyRepoCreateDTO createDto) {

        GHCreateDto ghCreateDto = new GHCreateDto(createDto.getName(), createDto.getDescription(), createDto.getOwnerName(), false);
        GHRepositoryResponse ghRepositoryResponse = githubClient.create(ghCreateDto);

        if (!ghRepositoryResponse.isOperationSuccessful()) {
            throw new BadRequestException(ghRepositoryResponse.responseMessage());
        }
        createDto.setLinkToGitHub(ghRepositoryResponse.URL());
        myRepoClient.create(createDto);
    }
}
