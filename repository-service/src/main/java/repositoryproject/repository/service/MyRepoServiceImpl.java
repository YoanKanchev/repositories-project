package repositoryproject.repository.service;

import org.springframework.stereotype.Service;
import repositoryproject.repository.model.MyRepo;
import repositoryproject.repository.model.MyRepoCreateDTO;
import repositoryproject.repository.repository.MyRepoRepository;

import java.util.List;

@Service
public record MyRepoServiceImpl(MyRepoRepository repository) implements MyRepoService {
    @Override
    public List<MyRepo> getAll() {
        return repository.findAll();
    }

    @Override
    public MyRepo create(MyRepoCreateDTO createDTO) {
        MyRepo newRepo = MyRepo.builder()
                .name(createDTO.getName())
                .description(createDTO.getDescription())
                .linkToGitHub(createDTO.getLinkToGitHub())
                .ownerName(createDTO.getOwnerName())
                .build();
        return repository.save(newRepo);
    }
}
