## Overview
Repositories project is a SPA web application witch functionality is creating and rating GitHub repositories.
It can also navigate to the created repositories directly in GitHub.

The application also has authentication/authorization mechanism using a Keycloak server.

## Installation

In order for the application to work properly there are needed a few steps of configuration after downloading the project:

1. Create and start containers from the docker-compose.yml file in the main directory
2. Setting up the database:
- Go to http://localhost:5050/ (this is the url for the PGAdmin service from the started containers) - it asks you for a password: "password" 
- Click on Add New Server and fill the fields with the folowing:

https://imgur.com/ZnakETV

https://imgur.com/jUThOL3

- Expand to Servers->some-random-name->Databases, right click on Databases and create 2 databases: myrepos and reviews

3. Setting up the Kyecloak server:

-  Go to http://localhost:8090/  
-  Go to Administration console - username: "admin" , password: "admin" 
-  https://imgur.com/PiX7pan selecting keyloak-settings.json from the main folder of the project
-  Next go to Clients -> repositories-app -> Credentials, regenerate the client Secret and copy it to your clipboard https://imgur.com/LlY1kz4
-  Go to the project in the api-gateway application.yml and change the client-secret property with the copied from the previous step https://imgur.com/04MJ2X1


After thease steps you can run all of the microservices and go to http://localhost:8080/

