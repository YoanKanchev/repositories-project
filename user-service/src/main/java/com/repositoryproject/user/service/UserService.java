package com.repositoryproject.user.service;

import com.repositoryproject.user.model.MyUser;

import java.util.List;

public interface UserService {
    List<MyUser> getAll();
}
