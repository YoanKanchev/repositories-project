package com.repositoryproject.frontend.clients.users;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MyUser {

    private Integer id;
    private String firstName;
    private String lastName;
    private String email;

}
