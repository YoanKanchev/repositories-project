package com.repositoryproject.review.service;

import com.repositoryproject.review.model.ReviewDto;
import com.repositoryproject.review.exceptions.EntityNotFoundException;
import com.repositoryproject.review.model.Review;
import com.repositoryproject.review.repository.ReviewRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public record ReviewServiceImpl(ReviewRepository repository) implements ReviewService {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    @Override
    public List<Review> getAllForRepository(int repositoryId) {
        return repository.findAllByRepositoryId(repositoryId);
    }

    @Override
    public void reviewRepository(ReviewDto reviewDto) {
        Review review = Review.builder()
                .repositoryId(reviewDto.getRepositoryId())
                .reviewer(reviewDto.getReviewer())
                .rating(reviewDto.getRating())
                .description(reviewDto.getDescription())
                .createDate(LocalDateTime.now().format(formatter))
                .build();
        repository.save(review);
    }

    @Override
    public void delete(int id) {
        Optional<Review> review = repository.findById(id);
        if (review.isEmpty()) {
            throw new EntityNotFoundException(String.format("Review with ID: %s not found!", id));
        }
        repository.delete(review.get());
    }
}
