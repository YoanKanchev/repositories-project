package com.repositoryproject.review.service;

import com.repositoryproject.review.model.Review;
import com.repositoryproject.review.model.ReviewDto;

import java.util.List;

public interface ReviewService {
    List<Review> getAllForRepository(int repositoryId);

    void reviewRepository(ReviewDto reviewDto);

    void delete(int id);
}
