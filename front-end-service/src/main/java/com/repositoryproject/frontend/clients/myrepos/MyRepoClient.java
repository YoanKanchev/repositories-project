package com.repositoryproject.frontend.clients.myrepos;

import com.repositoryproject.frontend.config.OAuthFeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "repository-service", configuration = OAuthFeignConfig.class, fallback = MyRepoClientFallback.class)
public interface MyRepoClient {

    @GetMapping("/my-repositories")
    List<MyRepo> getAll();

    @PostMapping("/my-repositories")
    MyRepo create(@RequestBody MyRepoCreateDTO createDto);
}
