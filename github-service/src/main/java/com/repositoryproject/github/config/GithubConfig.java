package com.repositoryproject.github.config;

import org.kohsuke.github.GitHub;
import org.kohsuke.github.GitHubBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.io.IOException;

@Configuration
public class GithubConfig {

    private final String oAuthToken;

    @Autowired
    public GithubConfig(Environment env) {
        this.oAuthToken = env.getProperty("github-token");
    }

    @Bean
    public GitHub getGithubConnection() throws IOException {
        return new GitHubBuilder().withOAuthToken(oAuthToken).build();
    }
}
