package com.repositoryproject.github.model;


public record GHCreateDto(String name, String description, String owner, Boolean isPrivate) {
}
