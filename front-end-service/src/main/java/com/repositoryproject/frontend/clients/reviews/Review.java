package com.repositoryproject.frontend.clients.reviews;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Review {
    private Integer id;
    private Integer repositoryId;
    private String reviewer;
    private Double rating;
    private String description;
    private String createDate;
}
